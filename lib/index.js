// Declare internals
var internals = {}

// Defaults
internals.defaults = {};

exports.register = function (plugin, options, next) {

    plugin.hapi.utils.assert(typeof plugin.route === 'function', 'Plugin permissions must allow route');

    var settings = plugin.hapi.utils.applyToDefaults(internals.defaults, options),
        server = plugin.servers[0];

    // 404 and 500 error handling
    server.ext('onPreResponse', function (request, reply) {
        var response = request.response;

        if (!response.isBoom) {
            return reply();
        }

        var error = response;

        if (error.output.statusCode === 500 || (request.route.path === '/{route*}' && error.output.statusCode === 404)) {
            var url = '/' + error.output.statusCode;

            //return reply('success').redirect(url);
            server.inject(url, function (res) {
                if (!res || !res.result || !res.output || res.output.statusCode === 404) {
                    return reply(res.result);
                }
                return reply(res.result);
            });
        }
        else {
            return reply(error.output.message);
        }
    });

    next();
};
